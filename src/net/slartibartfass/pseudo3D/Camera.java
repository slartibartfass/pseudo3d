package net.slartibartfass.pseudo3D;

import net.slartibartfass.pseudo3D.core.Vertex2D;
import net.slartibartfass.pseudo3D.core.Vertex3D;
import net.slartibartfass.pseudo3D.meshes.Mesh;
import org.apache.commons.math3.geometry.euclidean.threed.Line;
import org.apache.commons.math3.geometry.euclidean.threed.Plane;
import org.apache.commons.math3.geometry.euclidean.threed.Rotation;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

import java.util.HashSet;

public class Camera {

    private Plane viewScreen;
    private Vector3D vanishingPoint;
    private World world;

    public Camera(World world, Vector3D screenCenter, Vector3D vanishingPoint) {
        this.world = world;
        this.vanishingPoint = vanishingPoint;
        this.viewScreen = new Plane(screenCenter, new Vector3D(1, vanishingPoint, 1, screenCenter));
    }

    public HashSet<Vertex2D> getVerticesOnScreen() {
        return getVerticesOnScreen(world.meshes);
    }

    public HashSet<Vertex2D> getVerticesOnScreen(HashSet<Mesh> meshes) {
        HashSet<Vertex2D> verticesOnScreen = new HashSet<Vertex2D>();
        for (Mesh mesh : meshes) {
            verticesOnScreen.addAll(getVerticesOnScreen(mesh));
        }
        return verticesOnScreen;
    }

    public HashSet<Vertex2D> getVerticesOnScreen(Mesh mesh) {
        HashSet<Vertex2D> verticesOnScreen = new HashSet<Vertex2D>();
        for (Vertex3D vertice : mesh.vertices) {
            verticesOnScreen.add(getVertexOnScreen(mesh, vertice));
        }
        return verticesOnScreen;
    }

    private Vertex2D getVertexOnScreen(Mesh mesh, Vertex3D vertex3D) {
        Vector2D point1 = getPointOnScreen(mesh.origin.add(vertex3D.points[0]));
        Vector2D point2 = getPointOnScreen(mesh.origin.add(vertex3D.points[1]));
        Vector2D point3 = getPointOnScreen(mesh.origin.add(vertex3D.points[2]));
        return new Vertex2D(point1, point2, point3);
    }

    private Vector2D getPointOnScreen(Vector3D point) {
        Line vectorLine = new Line(point, vanishingPoint);
        Vector3D intersection = viewScreen.intersection(vectorLine);
        return viewScreen.toSubSpace(intersection);
    }

    public void translate(Vector3D translation) {
        viewScreen = viewScreen.translate(translation);
        vanishingPoint = vanishingPoint.add(translation);
    }

    public void rotate(Vector3D center, Rotation rotation) {
        viewScreen = viewScreen.rotate(center, rotation);
        vanishingPoint = rotation.applyTo(vanishingPoint);
    }

}
