package net.slartibartfass.pseudo3D;

import net.slartibartfass.pseudo3D.meshes.Cube;
import org.apache.commons.math3.geometry.euclidean.threed.Rotation;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

public class Main {

    public static void main(String[] args) {
        World world = new World();
        final Cube cube = new Cube(Vector3D.ZERO, 200);
        world.meshes.add(cube);
        final Keyboard keyboard = new Keyboard();
        final Screen screen = new Screen(world, 1200, 600);
        screen.addKeyListener(keyboard);
        JFrame window = initWindow();
        window.add(screen);
        window.pack();
        screen.requestFocus();
        final Rotation rotation = new Rotation(new Vector3D(1, 1, 1), Vector3D.PLUS_I, new Vector3D(1, 1, 1), new Vector3D(1, 0.01, 0));

        Timer updateTimer = new Timer(1000 / 60, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
//                if (keyboard.isKeyPressed(KeyEvent.VK_UP)) cube.translate(0, 0, -1);
//                if (keyboard.isKeyPressed(KeyEvent.VK_DOWN)) cube.translate(0, 0, 1);
//                if (keyboard.isKeyPressed(KeyEvent.VK_LEFT)) cube.translate(-1, 0, 0);
//                if (keyboard.isKeyPressed(KeyEvent.VK_RIGHT)) cube.translate(1, 0, 0);
                cube.rotate(Vector3D.ZERO, rotation);
            }
        });
        updateTimer.start();

        Thread graphicsLoop = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    screen.repaint();
                }
            }
        });
        graphicsLoop.start();
    }

    private static JFrame initWindow() {
        JFrame window = new JFrame();
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setTitle("Pseudo3D");
        window.setResizable(false);
        window.setVisible(true);
        return window;
    }

}
