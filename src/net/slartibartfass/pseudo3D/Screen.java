package net.slartibartfass.pseudo3D;

import net.slartibartfass.pseudo3D.core.Vertex2D;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

import javax.swing.*;
import java.awt.*;

public class Screen extends JPanel {

    public Camera camera;

    private int width, height;

    public Screen(World world, int width, int height) {
        this.width = width;
        this.height = height;
        this.camera = new Camera(world, new Vector3D(0, -20, 0), new Vector3D(0, -500, 0));
        this.setPreferredSize(new Dimension(width, height));
    }

    @Override
    protected void paintComponent(Graphics graphics) {
        Graphics2D graphics2D = (Graphics2D) graphics;
        super.paintComponent(graphics2D);
        graphics2D.setColor(Color.BLACK);
        graphics2D.setStroke(new BasicStroke(1));
        for (Vertex2D vertex : camera.getVerticesOnScreen()) {
            drawLine(vertex.points[0], vertex.points[1], graphics2D);
            drawLine(vertex.points[1], vertex.points[2], graphics2D);
            drawLine(vertex.points[2], vertex.points[0], graphics2D);
        }

    }

    private void drawLine(Vector2D pointA, Vector2D pointB, Graphics2D graphics) {
        int lineStartX = (int) pointA.getX() + width / 2;
        int lineStartY = (int) pointA.getY() + height / 2;
        int lineEndX = (int) pointB.getX() + width / 2;
        int lineEndY = (int) pointB.getY() + height / 2;
        graphics.drawLine(lineStartX, lineStartY, lineEndX, lineEndY);
    }


}
