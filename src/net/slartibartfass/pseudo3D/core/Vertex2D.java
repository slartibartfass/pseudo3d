package net.slartibartfass.pseudo3D.core;


import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

public final class Vertex2D {

    public Vector2D[] points;

    public Vertex2D(Vector2D point1, Vector2D point2, Vector2D point3) {
        points = new Vector2D[3];
        points[0] = point1;
        points[1] = point2;
        points[2] = point3;
    }
}
