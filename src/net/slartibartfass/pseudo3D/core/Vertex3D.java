package net.slartibartfass.pseudo3D.core;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

public final class Vertex3D {

    public Vector3D[] points;

    public Vertex3D(Vector3D point1, Vector3D point2, Vector3D point3) {
        points = new Vector3D[3];
        points[0] = point1;
        points[1] = point2;
        points[2] = point3;
    }
}
