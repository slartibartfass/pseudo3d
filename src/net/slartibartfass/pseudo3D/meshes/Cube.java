package net.slartibartfass.pseudo3D.meshes;

import net.slartibartfass.pseudo3D.core.Vertex3D;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

public class Cube extends Mesh {

    public double sideLength;

    public Cube(Vector3D origin, double sideLength) {
        super(origin);

        if (sideLength < 0) {
            throw new IllegalArgumentException("sideLength cannot be negative");
        }
        this.sideLength = sideLength;

        Vector3D point1 = new Vector3D(-sideLength/2, -sideLength/2, -sideLength/2);
        Vector3D point2 = new Vector3D(sideLength/2, -sideLength/2, -sideLength/2);
        Vector3D point3 = new Vector3D(sideLength/2, sideLength/2, -sideLength/2);
        Vector3D point4 = new Vector3D(-sideLength/2, sideLength/2, -sideLength/2);
        Vector3D point5 = new Vector3D(-sideLength/2, -sideLength/2, sideLength/2);
        Vector3D point6 = new Vector3D(sideLength/2, -sideLength/2, sideLength/2);
        Vector3D point7 = new Vector3D(sideLength/2, sideLength/2, sideLength/2);
        Vector3D point8 = new Vector3D(-sideLength/2, sideLength/2, sideLength/2);

        vertices.add(new Vertex3D(point1, point2, point3));
        vertices.add(new Vertex3D(point1, point3, point4));
        vertices.add(new Vertex3D(point2, point3, point7));
        vertices.add(new Vertex3D(point2, point6, point7));
        vertices.add(new Vertex3D(point1, point4, point8));
        vertices.add(new Vertex3D(point1, point5, point8));
        vertices.add(new Vertex3D(point1, point2, point6));
        vertices.add(new Vertex3D(point1, point5, point6));
        vertices.add(new Vertex3D(point3, point4, point8));
        vertices.add(new Vertex3D(point3, point7, point8));
        vertices.add(new Vertex3D(point5, point6, point7));
        vertices.add(new Vertex3D(point5, point8, point7));

    }
}
