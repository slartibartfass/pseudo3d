package net.slartibartfass.pseudo3D.meshes;

import net.slartibartfass.pseudo3D.core.Vertex3D;
import org.apache.commons.math3.geometry.euclidean.threed.Rotation;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import java.util.HashSet;

public class Mesh {

    public Vector3D origin;
    public HashSet<Vertex3D> vertices;

    public Mesh(Vector3D origin) {
        this.vertices = new HashSet<Vertex3D>();
        this.origin = origin;
    }

    public Mesh(Vector3D origin, Vertex3D... vertices) {
        this.vertices = new HashSet<Vertex3D>();
        this.origin = origin;
        for (Vertex3D vertex : vertices) {
            this.vertices.add(vertex);
        }
    }

    public void translate(double newX, double newY, double newZ) {
        double x = origin.getX() + newX;
        double y = origin.getY() + newY;
        double z = origin.getZ() + newZ;
        translate(new Vector3D(x, y, z));
    }

    public void translate(Vector3D translation) {
        origin = origin.add(translation);
    }

    public void rotate(final Vector3D center, final Rotation rotation) {
        Vector3D delta = origin.subtract(center);
        origin = center.add(rotation.applyTo(delta));

        for (Vertex3D vertex : vertices) {
            for (int i = 0; i < vertex.points.length; i++) {
                vertex.points[i] = rotation.applyTo(vertex.points[i]);
            }
        }
    }

}
