package net.slartibartfass.pseudo3D;

import org.apache.commons.math3.geometry.euclidean.threed.Line;
import org.apache.commons.math3.geometry.euclidean.threed.Plane;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.junit.Test;

public class PlaneTest {
    @Test
    public void testGetIntersectionPoint() throws Exception {
        Plane vectorPlane = new Plane(new Vector3D(9, 0, 0), new Vector3D(-9, 4.5, 0), new Vector3D(-9, 0, 4.5));
        Vector3D intersection = vectorPlane.intersection(new Line(new Vector3D(2, 3, 0), new Vector3D(1, -2, 2)));
        System.out.println(intersection.getX() + " " + intersection.getY() + " " + intersection.getZ());
    }
}
